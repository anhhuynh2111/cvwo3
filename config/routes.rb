Rails.application.routes.draw do
  resources :categories
  resources :tasks
  get 'pages/home'
  root 'categories#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
